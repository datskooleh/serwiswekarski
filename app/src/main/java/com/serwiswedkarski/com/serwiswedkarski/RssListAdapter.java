package com.serwiswedkarski.com.serwiswedkarski;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RssListAdapter extends ArrayAdapter<Message> {
    private List<Message> rssItems = null;
    private Context context;

    public RssListAdapter(Context context, int resource, ArrayList<Message> objects) {
        super(context, resource, objects);
        if (objects == null) {
            objects = new ArrayList<>();
        }

        this.rssItems = objects;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);

            convertView = inflater.inflate(R.layout.row_layout, null);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
        TextView tvContent = (TextView) convertView.findViewById(R.id.textViewContent);
        TextView tvDate = (TextView) convertView.findViewById(R.id.textViewDate);

        Message messageModel = rssItems.get(position);

        tvTitle.setText(messageModel.getTitle());
        tvContent.setText(messageModel.getDescription());
        tvDate.setText(messageModel.getDate().toString());

        return convertView;
    }
}
