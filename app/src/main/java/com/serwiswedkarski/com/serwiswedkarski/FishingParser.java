package com.serwiswedkarski.com.serwiswedkarski;

import android.app.Activity;
import android.os.AsyncTask;
import android.renderscript.ScriptGroup;
import android.text.Html;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.InvalidParameterException;
import java.text.ParseException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class FishingParser {
    public void parseAndAssign(RssListAdapter adapter) {
        URL url = null;
        try {
            url = new URL("http://www.fishing.pl/rss/feed/article/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (url == null) {
            throw new InvalidParameterException("Address does not response or is invalid");
        }

        Tasker tasker = new Tasker();
        tasker.execute(adapter, url);
    }

    private class Tasker extends AsyncTask<Object, Void, List<Message>> {
        RssListAdapter adapter = null;

        @Override
        protected List<Message> doInBackground(Object... objects) {
            if (objects == null || objects.length == 0) {
                throw new IllegalArgumentException("You must send one or more arguments to parse");
            }

            adapter = (RssListAdapter) objects[0];
            URL url = (URL) objects[1];

            InputStream is = null;
            List<Message> list = null;

            try {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setReadTimeout(15000);
                connection.setRequestMethod("GET");
                connection.connect();

                is = connection.getInputStream();

                list = parseXml(is);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return list;
        }

        private List<Message> parseXml(InputStream is) {
            List<Message> items = new ArrayList<>();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            Document doc = null;

            try {
                dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(is);

                NodeList nodesList = doc.getElementsByTagName("item");

                for (int i = 0; i < nodesList.getLength(); i++) {
                    Node node = nodesList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;
                        Message message = parseElementToMessage(element);
                        items.add(message);
                    }
                }
            } catch (ParserConfigurationException | SAXException | IOException | ParseException e) {
                e.printStackTrace();
            }

            return items;
        }

        private Message parseElementToMessage(Element element) throws ParseException {
            String title = element.getElementsByTagName("title")
                    .item(0)
                    .getTextContent();
            String description = Html.fromHtml(element.getElementsByTagName("description")
                    .item(0)
                    .getTextContent()).toString();
            String publicationDate = element.getElementsByTagName("pubDate").item(0).getTextContent().replace(" GMT", "");
            String link = element.getElementsByTagName("link").item(0).getTextContent();
            return new Message(title, description, link, publicationDate);
        }

        @Override
        protected void onPostExecute(List<Message> messages) {
            if (messages != null && messages.size() > 0) {
                for (Message m : messages) {
                    adapter.add(m);
                }
                adapter.notifyDataSetChanged();
            }

            //super.onPostExecute(messages);
        }
    }
}
