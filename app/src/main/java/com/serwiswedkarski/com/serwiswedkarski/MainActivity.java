package com.serwiswedkarski.com.serwiswedkarski;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ExpandedMenuView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lvFeeds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvFeeds = (ListView) findViewById(R.id.listViewFeeds);

        RssListAdapter adapter = new RssListAdapter(this, R.layout.row_layout, new ArrayList<Message>());
        lvFeeds.setAdapter(adapter);
        lvFeeds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Message item = (Message) adapterView.getItemAtPosition(i);

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(item.getLink()));
                startActivity(intent);
            }
        });
        FishingParser parser = new FishingParser();
        parser.parseAndAssign(adapter);
    }
}
