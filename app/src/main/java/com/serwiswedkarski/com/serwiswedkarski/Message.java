package com.serwiswedkarski.com.serwiswedkarski;

public class Message {
    private String title;
    private String description;
    private String date;
    private String link;

    public Message(String title, String description, String link, String date) {
        setTitle(title);
        setDescription(description);
        setLink(link);
        setDate(date);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
